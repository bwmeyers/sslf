#!/usr/bin/env python

import os
import logging
import pytest
import numpy as np

from sslf.sslf import Spectrum


TEST_DIR = '/'.join(os.path.realpath(__file__).split('/')[0:-1])


def test_rms_0():
    with pytest.raises(ValueError):
        Spectrum(np.zeros(100))


def test_lowest_logging_level():
    logging.basicConfig(level=logging.DEBUG)
    s = Spectrum(np.random.normal(size=1000))
    s.find_cwt_peaks(scales=np.arange(1, 20), snr=5)
    s.subtract_bandpass()


def test_invalid_types():
    with pytest.raises(ValueError):
        Spectrum({})
    with pytest.raises(ValueError):
        Spectrum(True)
    with pytest.raises(ValueError):
        Spectrum(np.arange(10), vel={})
    with pytest.raises(ValueError):
        Spectrum(np.arange(10), vel=True)
    # This raises no exception.
    Spectrum(np.arange(10), vel=np.arange(10, 20))


if __name__ == "__main__":
    # Introspect and run all the functions starting with "test".
    for f in dir():
        if f.startswith("test"):
            print(f)
            exec(f+"()")
